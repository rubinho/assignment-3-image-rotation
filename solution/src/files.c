#include "../include/files.h"

FILE *open_file_for_reading(const char *filename) {
    FILE* file = fopen(filename, "rb");
    return file;
}
FILE* create_file_for_writing(const char* filename) {
    FILE* file = fopen(filename, "wb");
    return file;
}

bool close_file(FILE* file) {
    return (fclose(file) == 0);
}
