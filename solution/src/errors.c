#include "../include/errors.h"

void check_status_error_and_exit(uint8_t status, uint8_t correct_value) {
    if (status != correct_value){
        exit(1);
    }
}

void check_file_error_and_exit(FILE *file, error_codes error_code){
    if (!file){
        print_error_message(error_code);
        exit(1);
    }
}

void check_bool_and_exit(bool status){
    if (!status){
        exit(1);
    }
}
