#include "../include/rotation.h"

static angles get_angle_by_numeric_value(int32_t numeric_angle) {
    size_t size = sizeof(numeric_angles) / sizeof(numeric_angles[0]);
    for (size_t i = 0; i < size; i++) {
        if (numeric_angles[i] == numeric_angle) {
            return (angles) i;
        }
    }

    return ANGLE_NOT_FOUND;

}

static struct maybe_image rotate_0(struct image *image) {

    struct image new_image;

    new_image.width = image->width;
    new_image.height = image->height;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            new_image.data[j * image->width + i] = image->data[j * image->width + i];
        }

    }

    return (struct maybe_image) {.image = new_image, .status = ROTATED_OK};
}

static struct maybe_image rotate_90(struct image *image) {
    struct image new_image;

    new_image.width = image->height;
    new_image.height = image->width;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            struct pixel current_pixel = image->data[j * image->width + i];

            uint32_t new_j = j;
            uint32_t new_i = new_image.height - i - 1;

            new_image.data[new_i * new_image.width + new_j] = current_pixel;
        }

    }

    return (struct maybe_image) {.image = new_image, .status = ROTATED_OK};
}

static struct maybe_image rotate_180(struct image *image) {
    struct image new_image;

    new_image.width = image->width;
    new_image.height = image->height;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            struct pixel current_pixel = image->data[j * image->width + i];

            uint32_t new_j = new_image.width - i - 1;
            uint32_t new_i = new_image.height - j - 1;

            new_image.data[new_i * new_image.width + new_j] = current_pixel;
        }

    }

    return (struct maybe_image) {.image = new_image, .status = ROTATED_OK};
}

static struct maybe_image rotate_270(struct image *image) {
    struct image new_image;

    new_image.width = image->height;
    new_image.height = image->width;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            struct pixel current_pixel = image->data[j * image->width + i];

            uint32_t new_j = new_image.width - j - 1;
            uint32_t new_i = i;

            new_image.data[new_i * new_image.width + new_j] = current_pixel;
        }

    }

    return (struct maybe_image) {.image = new_image, .status = ROTATED_OK};

}

static struct maybe_image (*rotations[])(struct image *) = {
        [ANGLE_ZERO] = rotate_0,
        [ANGLE_PLUS_90] = rotate_90,
        [ANGLE_PLUS_180] = rotate_180,
        [ANGLE_PLUS_270] = rotate_270,
        [ANGLE_MINUS_90] = rotate_270,
        [ANGLE_MINUS_180] = rotate_180,
        [ANGLE_MINUS_270] = rotate_90

};


struct maybe_image rotate(struct image img, int32_t numeric_angle) {
    angles angle = get_angle_by_numeric_value(numeric_angle);

    if (angle == ANGLE_NOT_FOUND) {
        print_error_message(ERROR_ROTATE);
        return (struct maybe_image) {.image = img, .status = ROTATED_ERROR};
    }

    struct maybe_image rotated_image = rotations[angle](&img);

    rotation_state status = rotated_image.status;

    if (status != ROTATED_OK) {
        print_error_message(ERROR_ROTATE);
        return (struct maybe_image) {.image = rotated_image.image, .status = status};
    }
    return (struct maybe_image) {.image = rotated_image.image, .status = ROTATED_OK};
}
