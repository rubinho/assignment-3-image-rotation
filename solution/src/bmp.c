#include "../include/bmp.h"


#define DEFAULT_BMP_FTYPE 0x4D42
#define DEFAULT_BMP_BISIZE 40
#define DEFAULT_BMP_PLANES 1
#define DEFAULT_BMP_BITCOUNT 24


static bool read_header(FILE *in, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, in);
}

static bool write_header(FILE *out, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out);
}

static read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header input_header = {0};
    if (!read_header(in, &input_header)) {
        return READ_INVALID_HEADER;
    }

    if (input_header.bfType != DEFAULT_BMP_FTYPE) {
        return READ_INVALID_SIGNATURE;
    }


    img->height = input_header.biHeight;
    img->width = input_header.biWidth;
    img->data = (struct pixel *) malloc(img->width * img->height * sizeof(struct pixel));

    if (!img->data){
        return READ_OUT_OF_MEMORY;
    }

    size_t padding = (4 - (img->width * sizeof(struct pixel) % 4)) % 4;

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            free(img->data);
            return READ_INVALID_BITS;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            free(img->data);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

static write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header output_header;

    output_header.bfType = DEFAULT_BMP_FTYPE;
    output_header.bfileSize = (img->width * sizeof(struct pixel)) * img->height + sizeof(output_header);
    output_header.bfReserved = 0;
    output_header.bOffBits = sizeof(output_header);
    output_header.biSize = DEFAULT_BMP_BISIZE;
    output_header.biWidth = img->width;
    output_header.biHeight = img->height;
    output_header.biPlanes = DEFAULT_BMP_PLANES;
    output_header.biBitCount = DEFAULT_BMP_BITCOUNT;
    output_header.biCompression = 0;
    output_header.biSizeImage = (img->width * sizeof(struct pixel)) * img->height;
    output_header.biXPelsPerMeter = 0;
    output_header.biYPelsPerMeter = 0;
    output_header.biClrUsed = 0;
    output_header.biClrImportant = 0;

    if (write_header(out, &output_header) != 1) {
        free(img->data);
        return WRITE_INVALID_HEADER;
    }


    for (int32_t i = 0; i < img->height; i++) {
        for (int32_t j = 0; j < img->width; j++) {
            struct pixel current_pixel = img->data[i * img->width + j];

            if (fwrite(&current_pixel, sizeof(struct pixel), 1, out) != 1) {
                free(img->data);
                return WRITE_INVALID_IMAGE;
            }
        }

        size_t padding = (4 - (img->width * sizeof(struct pixel) % 4)) % 4;
        if (padding > 0) {
            int8_t padding_byte = 0;
            for (size_t k = 0; k < padding; k++) {
                if (fwrite(&padding_byte, 1, 1, out) != 1) {
                    free(img->data);
                    return WRITE_INVALID_IMAGE;
                }
            }
        }
    }


    return WRITE_OK;
}

static void throw_error(error_codes error_code) {
    print_error_message(error_code);
}

read_status read_bmp_from_file(struct bmp_image img) {

    FILE* input_file = img.bmp_file;
    struct image* input_image = img.image;

    uint8_t status = from_bmp(input_file, input_image);
    if (status != READ_OK) {
        throw_error(ERROR_READ_FILE);
        return status;
    }

    if (!close_file(input_file)) {
        throw_error(ERROR_CLOSE_FILE);
        return READ_ERROR_CLOSE;
    }

    return READ_OK;
}

write_status write_bmp_to_file(FILE *out_file, struct image *out_image) {
    if (!out_file) {
        throw_error(ERROR_CREATE_FILE);
        return WRITE_ERROR_CREATE;
    }

    uint8_t status = to_bmp(out_file, out_image);

    if (status != WRITE_OK) {
        throw_error(ERROR_WRITE_FILE);
        return status;
    }


    if (!close_file(out_file)) {
        throw_error(ERROR_CLOSE_FILE);
        return WRITE_ERROR_CLOSE;
    }

    return WRITE_OK;
}
