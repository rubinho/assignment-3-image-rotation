#include "../include/util.h"

#define MIN_ANGLE (-270)
#define MAX_ANGLE 270
#define FREQUENCY 90

void print_error_message(error_codes error_code) {
    fprintf(stderr, "%s", error_messages[error_code]);
}

bool is_valid_angle(int32_t angle) {
    return (angle >= MIN_ANGLE && angle <= MAX_ANGLE && angle % FREQUENCY == 0);
}

struct maybe_angle parse_angle(char *string) {
    char *endPtr = "";
    int32_t angle = (int32_t) strtol(string, &endPtr, 10);

    if (*endPtr != '\0') {
        print_error_message(ERROR_PARSE_ANGLE);
        return (struct maybe_angle) {.is_correct = false};
    }
    bool status = is_valid_angle(angle);
    if (!status){
        print_error_message(ERROR_INVALID_ANGLE);
    }
    return (struct maybe_angle) {.angle = angle, .is_correct = status};
}
