#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h"
#include "../include/errors.h"
#include "../include/rotation.h"

int main(int argc, char **argv) {
    if (argc != 4) {
        print_error_message(ERROR_ARGS_CNT);
        return 1;
    }


    FILE *input_file = open_file_for_reading(argv[1]);
    check_file_error_and_exit(input_file, ERROR_FILE_OPEN);

    struct image input_image = {0};

    struct bmp_image img = {.image=&input_image, .bmp_file=input_file};
    uint8_t status = read_bmp_from_file(img);

    check_status_error_and_exit(status, READ_OK);

    struct maybe_angle parsed_angle = parse_angle(argv[3]);

    check_bool_and_exit(parsed_angle.is_correct);

    FILE *out_file = create_file_for_writing(argv[2]);

    check_file_error_and_exit(out_file, ERROR_CLOSE_FILE);

    struct maybe_image maybe_out_image = rotate(input_image, parsed_angle.angle);

    check_status_error_and_exit(maybe_out_image.status, ROTATED_OK);

    struct image out_image = maybe_out_image.image;


    status = write_bmp_to_file(out_file, &out_image);

    check_status_error_and_exit(status, WRITE_OK);


    free(input_image.data);
    free(out_image.data);

    printf("The picture is rotated by %" PRId32 " degrees", parsed_angle.angle);
    return 0;
}
