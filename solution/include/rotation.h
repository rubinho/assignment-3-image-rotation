#ifndef TEST_ROTATION_H
#define TEST_ROTATION_H

#include "../include/image.h"
#include "../include/util.h"
#include <inttypes.h>



typedef enum {
    ANGLE_MINUS_270 = 0,
    ANGLE_MINUS_180,
    ANGLE_MINUS_90,
    ANGLE_ZERO,
    ANGLE_PLUS_90,
    ANGLE_PLUS_180,
    ANGLE_PLUS_270,
    ANGLE_NOT_FOUND
} angles;

static const int32_t numeric_angles[] = {-270, -180, -90, 0, 90, 180, 270};


typedef enum {
    ROTATED_OK = 0,
    ROTATED_ERROR,
} rotation_state;

struct maybe_image {
    struct image image;
    rotation_state status;
};

struct maybe_image rotate(struct image img, int32_t numeric_angle);

#endif
