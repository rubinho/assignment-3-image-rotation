#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef enum {
    ERROR_ARGS_CNT = 0,
    ERROR_FILE_OPEN,
    ERROR_READ_FILE,
    ERROR_CLOSE_FILE,
    ERROR_PARSE_ANGLE,
    ERROR_CREATE_FILE,
    ERROR_ROTATE,
    ERROR_INVALID_ANGLE,
    ERROR_WRITE_FILE,
} error_codes;

struct maybe_angle {
    int32_t angle;
    bool is_correct;
};


static const char *const error_messages[] = {
        [ERROR_ARGS_CNT] = "Using the program: ./image-transformer <source-image> <transformed-image> <angle>",
        [ERROR_FILE_OPEN] = "File opening error. Make sure the path and permissions are correct",
        [ERROR_READ_FILE] = "File reading error. Please check the file structure",
        [ERROR_CLOSE_FILE] = "File closing error",
        [ERROR_PARSE_ANGLE] = "Angle parsing error",
        [ERROR_CREATE_FILE] = "Creating result file error",
        [ERROR_ROTATE] = "Rotating image error",
        [ERROR_INVALID_ANGLE] = "Error: invalid angle",
        [ERROR_WRITE_FILE] = "File writing error",
};

void print_error_message(error_codes error_code);

struct maybe_angle parse_angle(char *string);

#endif
