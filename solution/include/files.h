#ifndef IMAGE_TRANSFORMER_FILES_H
#define IMAGE_TRANSFORMER_FILES_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>


typedef enum {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    READ_ERROR_CLOSE,
    READ_OUT_OF_MEMORY,

} read_status;

typedef enum {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_IMAGE,
    WRITE_ERROR_CREATE,
    WRITE_ERROR_CLOSE,
} write_status;


FILE *open_file_for_reading(const char *filename);

FILE *create_file_for_writing(const char *filename);

bool close_file(FILE *file);

#endif
