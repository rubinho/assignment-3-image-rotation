#ifndef IMAGE_TRANSFORMER_ERRORS_H
#define IMAGE_TRANSFORMER_ERRORS_H

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/util.h"


void check_status_error_and_exit(uint8_t status, uint8_t correct_value);

void check_file_error_and_exit(FILE *file, error_codes error_code);

void check_bool_and_exit(bool status);

#endif
